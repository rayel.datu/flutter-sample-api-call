// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_app/pojo/country.dart';
import 'package:flutter_app/api/country_api.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:flutter_app/main.dart';

void main() {
  final CountryApi countryApi = CountryApi();
  test("country api call - get countries", () async {
    final countries = await countryApi.getAllCountries();
    expect(countries is List<Country>, true);
  });
}
