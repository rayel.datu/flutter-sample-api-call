import 'package:flutter/material.dart';
import 'package:flutter_app/bloc/settings_bloc.dart';
import 'package:flutter_app/ui/pages/country_list.dart';
import 'package:flutter_app/ui/pages/home.dart';
import 'package:flutter_app/ui/pages/region_list.dart';
import 'package:flutter_app/ui/pages/settings.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<SettingsBloc>(
          create: (_) => SettingsBloc(),
        )
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        onGenerateRoute: (settings) {
          switch (settings.name) {
            case Routes.regionList:
              return MaterialPageRoute(
                builder: (_) => RegionList(),
              );
            case Routes.countryList:
              return MaterialPageRoute(
                builder: (_) => CountryList(settings.arguments),
              );
            case Routes.settings:
              return MaterialPageRoute(
                builder: (_) => Settings(),
              );
            case Routes.home:
              return MaterialPageRoute(
                builder: (_) => Home(),
              );
            default:
              return null;
          }
        },
        initialRoute: Routes.home,
      ),
    );
  }
}

class Routes {
  static const String regionList = "/region-list";
  static const String countryList = "/country-list";
  static const String settings = "/settings";
  static const String home = "/home";
}
