import 'package:flutter/material.dart';
import 'package:flutter_app/pojo/country.dart';

class CountryListItem extends StatelessWidget {
  final Country _country;

  CountryListItem(
    this._country,
  );

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(12),
          child: Row(
            children: <Widget>[
              Container(
                width: 64,
                height: 64,
                padding: EdgeInsets.all(12),
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: Colors.grey),
                child: Image.network(
                  "https://www.countryflags.io/${_country.alpha2Code.toLowerCase()}/flat/64.png",
                  fit: BoxFit.fitWidth,
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(right: 12.0),
                  child: Text(
                    '${_country.name}',
                    style: TextStyle(fontSize: 20),
                    textAlign: TextAlign.center,
                    softWrap: true,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
