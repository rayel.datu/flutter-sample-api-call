import 'package:flutter/material.dart';

abstract class BaseWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
