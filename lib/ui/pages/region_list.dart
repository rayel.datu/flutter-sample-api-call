import 'package:flutter/material.dart';
import 'package:flutter_app/main.dart';
import 'package:flutter_app/ui/pages/country_list.dart';

class RegionList extends StatelessWidget {
  final _regions = ['Africa', 'Americas', 'Asia', 'Europe', 'Oceania'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemCount: _regions.length,
        itemBuilder: (context, index) => RegionListItem(
          _regions[index],
          onPressed: () {
            Navigator.of(context).pushNamed(Routes.countryList,
                arguments: CountryListArgs({'regions': _regions[index]}));
          },
        ),
      ),
    );
  }
}

class RegionListItem extends StatelessWidget {
  final String _region;
  final VoidCallback onPressed;

  const RegionListItem(
    this._region, {
    Key key,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Material(
        child: Card(
          child: Container(
            padding: EdgeInsets.all(12),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    '$_region',
                    style: TextStyle(fontSize: 20),
                    textAlign: TextAlign.center,
                  ),
                ),
                Icon(
                  Icons.keyboard_arrow_right,
                  color: Colors.grey,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
