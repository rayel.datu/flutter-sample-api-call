import 'package:flutter/material.dart';
import 'package:flutter_app/api/country_api.dart';
import 'package:flutter_app/bloc/country_list_bloc.dart';
import 'package:flutter_app/error_message.dart';
import 'package:flutter_app/ui/base_widget.dart';
import 'package:flutter_app/ui/dialogs/loading.dart';
import 'package:flutter_app/ui/widgets/country_item.dart';
import 'package:provider/provider.dart';

class CountryListArgs {
  final Map<String, String> filter;

  CountryListArgs(this.filter);
}

class CountryList extends StatelessWidget {
  final CountryListArgs _args;

  const CountryList(
    this._args, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => CountryListBloc(CountryApi(), _args.filter),
      child: CountryListWidget(_args.filter),
    );
  }
}

class CountryListWidget extends BaseWidget {
  final Map<String, String> _filter;

  CountryListWidget(this._filter);

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<CountryListBloc>(context);

    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (provider.state.shouldLoadFirst) {
        showDialog(
            context: context,
            child: LoadingDialog(),
            barrierDismissible: false);
        provider.getList(this._filter).then((_) {
          Navigator.pop(context);
        });
      }
    });

    return Scaffold(
      body: RefreshIndicator(
        onRefresh: () => provider.getList(this._filter),
        child: provider.state.hasError
            ? SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(
                  parent: ClampingScrollPhysics(),
                ),
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  child: Center(child: ErrorMessage()),
                ),
              )
            : ListView.builder(
                itemBuilder: (context, index) =>
                    CountryListItem(provider.state.countries[index]),
                itemCount: provider.state.countries.length,
              ),
      ),
    );
  }
}
