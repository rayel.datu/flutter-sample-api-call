import 'package:flutter/material.dart';
import 'package:flutter_app/bloc/settings_bloc.dart';
import 'package:flutter_app/ui/dialogs/loading.dart';
import 'package:provider/provider.dart';

class Settings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<SettingsBloc>(context);
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Center(
            child: Container(
              child: Row(
                children: <Widget>[
                  Checkbox(
                    onChanged: (isChecked) {
                      showDialog(
                        context: context,
                        child: LoadingDialog(),
                        barrierDismissible: false,
                      );
                      provider.toggleTest().then((_) => Navigator.pop(context));
                    },
                    value: provider.state.isTestChecked,
                  ),
                  Text('Test')
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
