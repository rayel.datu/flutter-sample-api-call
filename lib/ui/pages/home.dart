import 'package:flutter/material.dart';
import 'package:flutter_app/main.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          FlatButton(
            child: Text('Regions'),
            onPressed: () {
              Navigator.pushNamed(context, Routes.regionList);
            },
          ),
          FlatButton(
            child: Text('Settings'),
            onPressed: () {
              Navigator.pushNamed(context, Routes.settings);
            },
          )
        ],
      ),
    );
  }
}
