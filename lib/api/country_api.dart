import 'dart:convert' as convert;
import 'package:flutter_app/pojo/country.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class CountryApi {
  final String baseUrl = 'https://restcountries.eu/rest/v2';

  Future<List<Country>> getAllCountries() async {
    final url = '$baseUrl/all';
    var response = await http.get(url);
    if (response.statusCode == 200) {
      return await processResponse(response);
    } else {
      return Future.error("Connection Error");
    }
  }

  Future<List<Country>> getCountriesByRegion(String region) async {
    final url = '$baseUrl/region/${region.toLowerCase()}';
    var response = await http.get(url);
    if (response.statusCode == 200) {
      return await processResponse(response);
    } else {
      return Future.error("Connection Error");
    }
  }

  Future<List<Country>> processResponse(Response response) async {
    final List jsonResponse = await convert.jsonDecode(response.body);
    return jsonResponse.map((e) => Country.fromJson(e)).toList();
  }
}
