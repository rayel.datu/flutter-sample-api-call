import 'package:flutter/material.dart';

class SettingsState {
  bool isTestChecked = false;
  bool isLoading = false;
}

class SettingsBloc with ChangeNotifier {
  SettingsState state;

  SettingsBloc() {
    state = SettingsState();
  }

  Future toggleTest() async {
    await fakeApiCall();

    state.isTestChecked = !state.isTestChecked;
    notifyListeners();
  }

  Future fakeApiCall() async {
    await Future.delayed(Duration(seconds: 5));
    return;
  }
}
