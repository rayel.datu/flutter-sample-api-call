import 'package:flutter/cupertino.dart';
import 'package:flutter_app/pojo/country.dart';
import 'package:flutter_app/api/country_api.dart';

class CountryListState {
  bool hasError = false;
  bool shouldLoadFirst = true;
  List<Country> _countries = [];

  List<Country> get countries => _countries;
}

class CountryListBloc with ChangeNotifier {
  final CountryApi _api;
  CountryListState _state;

  CountryListBloc(this._api, Map<String, String> filter) {
    _state = CountryListState();
  }

  CountryListState get state => _state;

  Future getList(Map<String, String> filter) async {
    _state.hasError = false;

    switch (filter.keys.toList()[0]) {
      case 'regions':
        _state._countries = await _api
            .getCountriesByRegion(filter['regions'])
            .catchError((error) {
          print(error);
          _state.hasError = true;
        });
        break;
      default:
        _state._countries = await _api.getAllCountries().catchError((error) {
          print(error);
          _state.hasError = true;
        });
        break;
    }

    _state.shouldLoadFirst = false;
    notifyListeners();
    return;
  }
}
